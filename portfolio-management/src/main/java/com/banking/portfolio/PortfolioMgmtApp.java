package com.banking.portfolio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortfolioMgmtApp {
	public static void main(String[] args) {
		SpringApplication.run(PortfolioMgmtApp.class, args);
	}
}
